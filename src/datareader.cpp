#include "datareader.hpp"
#include <vector>
#include <sstream>
#include <string>
#include <iomanip>
#include <windows.h>
#include <ntsecapi.h>

using namespace std;

const wstring DataReader::m_map_file = L"IRSDKMemMapFileName";

string DataReader::Get(const string& var_name, size_t index) const
{
    auto elem = m_name_to_offset_and_type.find(var_name);
    ostringstream oss;
    if (elem != m_name_to_offset_and_type.end()) {
        int offset = elem->second.first;
        int type = elem->second.second;
        if (index) {
            offset += static_cast<int>(IRTypeSizes[type] * index);
        }
        char* address = m_file_buffer + GetHeader().buffer[0].offset + offset;

        switch (type) {
        case IRChar:
            oss << *reinterpret_cast<char*>(address);
            break;
        case IRBool:
            oss << *reinterpret_cast<bool*>(address);
            break;
        case IRInt:
            oss << *reinterpret_cast<int*>(address);
            break;
        case IRBitField:
            oss.fill('0');
            oss << "0x" << hex << setw(8) << *reinterpret_cast<int*>(address);
            break;
        case IRFloat:
            oss << *reinterpret_cast<float*>(address);
            break;
        case IRDouble:
            oss << *reinterpret_cast<double*>(address);
            break;
        }
    }

    return oss.str();
}

string DataReader::GetName(size_t index) const
{
    return m_cached_metadata[index].name;
}

int DataReader::GetCount(size_t index) const
{
    return m_cached_metadata[index].count;
}

int DataReader::GetType(size_t index) const
{
    return m_cached_metadata[index].type;
}

string DataReader::Get(size_t index, size_t sub_index) const
{
    return Get(m_cached_metadata[index].name, sub_index);
}

IRHeader DataReader::GetHeader() const
{
    return *(reinterpret_cast<IRHeader*>(m_file_buffer));
}

IRVariableHeader DataReader::GetVariableHeader(size_t index) const
{
    return m_cached_metadata[index];
}

string DataReader::GetSessionInfo() const
{
    if (m_file_buffer) {
        return m_file_buffer + GetHeader().session_info_offset;
    } else {
        return "";
    }
}

const vector<IRVariableHeader>& DataReader::GetVariableMetadata() const
{
    return m_cached_metadata;
}

bool DataReader::IsInitialized() const
{
    return m_file_handle != NULL;
}

void DataReader::Initialize()
{
    FindTelemetryFile();

    if (m_file_handle) {
        m_file_buffer = reinterpret_cast<char*>(MapViewOfFile(
            m_file_handle,
            FILE_MAP_READ,
            0,
            0,
            0
            ));

        for (size_t i = 0 ; i != static_cast<size_t>(GetHeader().variable_count) ; ++i) {
            IRVariableHeader variable_header = *reinterpret_cast<IRVariableHeader*>((m_file_buffer + GetHeader().variable_offset) + sizeof(IRVariableHeader) * i);
            m_cached_metadata.push_back(variable_header);
            m_name_to_offset_and_type[variable_header.name] = make_pair(variable_header.offset, variable_header.type);
        }

    }
}

//================================================================================

void DataReader::Cleanup()
{
    UnmapViewOfFile(m_file_buffer);
    CloseHandle(m_file_handle);
}

void DataReader::FindTelemetryFile()
{
    wostringstream file_name;
    file_name << L"Local\\" << m_map_file;
    m_file_handle = OpenFileMappingW(
        FILE_MAP_READ,
        FALSE,
        file_name.str().c_str()
        );

    if (NULL == m_file_handle) {
        ULONG session_count;
        LUID* luid_list;

        LsaEnumerateLogonSessions(
            &session_count,
            &luid_list
            );

        for (ULONG i = 0; i != session_count; ++i) {
            SECURITY_LOGON_SESSION_DATA* logon_session_data;
            LsaGetLogonSessionData(
                &luid_list[i],
                &logon_session_data
                );

            if (logon_session_data->LogonType == Interactive) {
                file_name.str(L"");
                file_name.clear();
                file_name << L"Session\\" << logon_session_data->Session << L"\\" << m_map_file;
                m_file_handle = OpenFileMappingW(
                    FILE_MAP_READ,
                    FALSE,
                    file_name.str().c_str()
                    );
                if (NULL != m_file_handle) { break; }
            }
        }
    }
}
