#include "datareader.hpp"

#pragma warning(push)
#pragma warning(disable: 4146 4127 4512)
#include "yaml-cpp/yaml.h"
#pragma warning(pop)

#include <sstream>
#include <vector>

using namespace std;

void Indent(ostringstream& oss, size_t level)
{
    for (size_t i = 0; i != level; ++i) {
        oss << "\t";
    }
}

string ProcessValueXml(const YAML::Node& n, size_t level, const string& parent = "")
{
    ostringstream xml;
    if (n.IsMap()) {
        for (YAML::const_iterator i = begin(n); i != end(n); ++i) {
            Indent(xml, level);
            xml << "<" << i->first;
            if (i->second.IsScalar()) {
                xml << " value=\"" << i->second << "\"/>\n";
            } else if (i->second.IsSequence() || i->second.IsMap()) {
                xml << ">\n";
                xml << ProcessValueXml(i->second, level + 1, i->first.as<string>());
                Indent(xml, level);
                xml << "</" << i->first << ">\n";
            }
        }
    } else if (n.IsSequence()) {
        for (YAML::const_iterator i = begin(n); i != end(n); ++i) {
            Indent(xml, level);
            if (parent.size() && *(parent.rbegin()) == 's') {
                string temp(begin(parent), end(parent) - 1);
                xml << "<" << temp;
            } else {
                xml << "<Value";
            }
            vector<YAML::const_iterator> complex_nodes;
            for (YAML::const_iterator j = begin(*i); j != end(*i); ++j) {
                if (j->second.IsMap() || j->second.IsSequence()) {
                    complex_nodes.push_back(j);
                } else {
                    xml << " " << j->first << "=\"" << j->second << "\"";
                }
            }
            if (!complex_nodes.size()) {
                xml << "/>\n";
            } else {
                xml << ">\n";
                for (auto j = begin(complex_nodes); j != end(complex_nodes); ++j) {
                    Indent(xml, level + 1);
                    xml << "<" << (*j)->first << ">\n";
                    xml << ProcessValueXml((*j)->second, level + 2, (*j)->first.as<string>());
                    Indent(xml, level + 1);
                    xml << "</" << (*j)->first << ">\n";
                }
                Indent(xml, level);
                if (parent.size() && *(parent.rbegin()) == 's') {
                    string temp(begin(parent), end(parent) -1);
                    xml << "</" << temp << ">\n";
                } else {
                    xml << "</Value>\n";
                }
            }
        }
    }
    return xml.str();
}

string GetXmlForSession(const string& session_info)
{
    ostringstream xml;
    xml << "\t<Session>\n";
    YAML::Node root = YAML::Load(session_info);
    for (YAML::const_iterator i = begin(root); i != end(root); ++i) {
        xml << "\t\t<" << i->first << ">\n"
            << ProcessValueXml(i->second, 3)
            << "\t\t</" << i->first << ">\n";

    }
    xml << "\t</Session>";
    return xml.str();
}

string ProcessValueJson(const pair<YAML::Node, YAML::Node>&, size_t);
string ProcessMapValueJson(const YAML::Node&, size_t);

string ProcessScalarOrNullValueJson(const pair<YAML::Node, YAML::Node>& value, size_t level)
{
    ostringstream json;
    Indent(json, level);
    json << "\"" << value.first << "\":\"" << value.second << "\"";
    return json.str();
}

string ProcessSequenceJson(const YAML::Node& node, size_t level)
{
    ostringstream json;
    Indent(json, level);
    for (auto i = begin(node); i != end(node); ++i) {
        if (i != begin(node)) {
            json << ", ";
        }
        // TODO: understand this better
        json << ProcessMapValueJson(*i, level + 1);
    }
    return json.str();
}

string ProcessSequenceValueJson(const pair<YAML::Node, YAML::Node>& value, size_t level)
{
    ostringstream json;
    Indent(json, level);
    json << "\"" << value.first << "\":[\n";
    json << ProcessSequenceJson(value.second, level + 1);
    json << "\n";
    Indent(json, level);
    json << "]";
    return json.str();
}

string ProcessMapValueJson(const YAML::Node& node, size_t level)
{
    ostringstream json;
    json << "{\n";
    for (auto i = begin(node); i != end(node); ++i) {
        if (i != begin(node)) {
            json << ",\n";
        }
        json << ProcessValueJson(*i, level + 1);
    }
    json << "\n";
    Indent(json, level);
    json << "}";

    return json.str();
}

string ProcessMapValueJson(const pair<YAML::Node, YAML::Node>& value, size_t level)
{
    ostringstream json;
    Indent(json, level);
    json << "\"" << value.first << "\":";
    json << ProcessMapValueJson(value.second, level);
    return json.str();
}

string ProcessValueJson(const pair<YAML::Node, YAML::Node>& value, size_t level)
{
    ostringstream json;

    switch (value.second.Type()) {
    case YAML::NodeType::Null:
    case YAML::NodeType::Scalar:
        json << ProcessScalarOrNullValueJson(value, level);
        break;
    case YAML::NodeType::Sequence:
        json << ProcessSequenceValueJson(value, level);
        break;
    case YAML::NodeType::Map:
        json << ProcessMapValueJson(value, level);
        break;
    }
    return json.str();
}

string GetJsonForSession(const string& session_info)
{
    ostringstream json;

    json << "\t\"Session\":{\n";
    YAML::Node node = YAML::Load(session_info);
    for (YAML::const_iterator i = begin(node); i != end(node); ++i) {
        if (i != begin(node)) {
            json << ",";
        }
        json << ProcessValueJson(*i, 2);
        json << "\n";
    }
    json << "\t}\n";

    return json.str();
}
