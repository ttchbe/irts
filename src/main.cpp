#define _CRT_SECURE_NO_WARNINGS

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#undef WIN32_LEAN_AND_MEAN


//#include <strsafe.h>
//#include <stdlib.h>

#include <http.h>

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <thread>
#include <algorithm>

#include "datareader.hpp"
#include "http.hpp"
#include "telemetry.hpp"

using namespace std;
using namespace irts;

extern VOID SvcInstall();
extern VOID __stdcall DoDeleteSvc();

SERVICE_STATUS        g_svc_status;
SERVICE_STATUS_HANDLE g_svc_status_handle;
HANDLE                g_svc_stop_event = NULL;

#define SVC_ERROR                        ((DWORD)0xC0020001L)

VOID SvcReportEvent(wchar_t* function)
{
    HANDLE eventsource_handle;
    vector<wchar_t> buffer;

    eventsource_handle = RegisterEventSourceW(NULL, L"irts");

    if (NULL != eventsource_handle) {
        wostringstream ss;
        ss << function << L" failed with " << GetLastError();
        wstring temp(L"irts");
        copy(begin(temp), end(temp), back_inserter(buffer));
        buffer.push_back(L'\0');
        copy(begin(ss.str()), end(ss.str()), back_inserter(buffer));
        buffer.push_back(L'\0');

        const wchar_t* strings = &buffer[0];

        ReportEventW(
            eventsource_handle,
            EVENTLOG_ERROR_TYPE,
            0,
            SVC_ERROR,
            NULL,
            2,
            0,
            &strings,
            NULL
            );

        DeregisterEventSource(eventsource_handle);
    }
}

VOID ReportSvcStatus(DWORD current_state, DWORD exit_code, DWORD wait_hint)
{
    static DWORD checkpoint = 1;

    g_svc_status.dwCurrentState = current_state;
    g_svc_status.dwWin32ExitCode = exit_code;
    g_svc_status.dwWaitHint = wait_hint;

    if (current_state == SERVICE_START_PENDING) {
        g_svc_status.dwControlsAccepted = 0;
    } else {
        g_svc_status.dwControlsAccepted = SERVICE_ACCEPT_STOP;
    }

    if (current_state == SERVICE_RUNNING || current_state == SERVICE_STOPPED) {
        g_svc_status.dwCheckPoint = 0;
    } else {
        g_svc_status.dwCheckPoint = checkpoint++;
    }

    SetServiceStatus(g_svc_status_handle, &g_svc_status);
}

VOID WINAPI SvcCtrlHandler(DWORD ctrl)
{
    switch (ctrl) {
    case SERVICE_CONTROL_STOP:
        ReportSvcStatus(SERVICE_STOP_PENDING, NO_ERROR, 0);

        SetEvent(g_svc_stop_event);
        ReportSvcStatus(g_svc_status.dwCurrentState, NO_ERROR, 0);
        break;
    case SERVICE_CONTROL_INTERROGATE:
        break;
    default:
        break;
    }
}

void DoWork()
{
    try {
        HttpListener<TelemetryRequestHandler> xml_listener(L"http://+:5678/irts", true);
    } catch (const exception& e) {
        cout << e.what() << endl;
    }
}


VOID SvcInit(DWORD, wchar_t**)
{
    g_svc_stop_event = CreateEvent(
        NULL,
        TRUE,
        FALSE,
        NULL
        );

    if (NULL == g_svc_stop_event) {
        ReportSvcStatus(SERVICE_STOPPED, NO_ERROR, 0);
        return;
    }

    ReportSvcStatus(SERVICE_RUNNING, NO_ERROR, 0);

    for (;;) {
        thread worker(DoWork);
        worker.detach();

        WaitForSingleObject(g_svc_stop_event, INFINITE);

        ReportSvcStatus(SERVICE_STOPPED, NO_ERROR, 0);
        return;
    }
}

VOID WINAPI SvcMain(DWORD argc, wchar_t *argv[])
{
    g_svc_status_handle = RegisterServiceCtrlHandlerW(
        L"irts",
        SvcCtrlHandler
        );

    if (NULL == g_svc_status_handle) {
        SvcReportEvent(L"RegisterServiceCtrlHandler");
        return;
    }

    g_svc_status.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
    g_svc_status.dwServiceSpecificExitCode = 0;

    ReportSvcStatus(SERVICE_START_PENDING, NO_ERROR, 3000);

    SvcInit(argc, argv);
}

int wmain(int argc, wchar_t* argv[])
{
    if (argc > 1) {
        if (lstrcmpiW(argv[1], L"install") == 0) {
            SvcInstall();
            return 0;
        } else if (lstrcmpiW(argv[1], L"uninstall") == 0) {
            DoDeleteSvc();
            return 0;
        } else if (lstrcmpiW(argv[1], L"debug") == 0) {
            DoWork();
            return 0;
        }
    }

    SERVICE_TABLE_ENTRYW DispatchTable[] =
    {
        { L"irts", (LPSERVICE_MAIN_FUNCTIONW)SvcMain },
        { NULL, NULL }
    };

    if (!StartServiceCtrlDispatcherW(DispatchTable)) {
        SvcInit(argc, argv);
        SvcReportEvent(L"StartServiceCtrlDispatcher");
    }

}

