#ifndef IRTS_DATAREADER_HPP
#define IRTS_DATAREADER_HPP

#include <windows.h>
#include <vector>
#include <string>
#include <unordered_map>

const int IRSDK_MAX_BUFS = 4;
const int IRSDK_MAX_STRING = 32;
const int IRSDK_MAX_DESCRIPTION = 64;

enum IRType
{
    IRChar = 0,
    IRBool,
    IRInt,
    IRBitField,
    IRFloat,
    IRDouble,
    IREnd
};

const std::string IRSessionState[] =
{
    "Invalid",
    "GetInCar",
    "Warmup",
    "ParadeLaps",
    "Racing",
    "Checkered",
    "Cooldown"
};

const size_t IRTypeSizes[IREnd] =
{
    1, // IRChar
    1, // IRBool
    4, // IRInt
    4, // IRBitField
    4, // IRFloat
    8  // IRDouble
};

struct IRVariableBuffer
{
    int tick_count;
    int offset;
    int padding[2];
};

struct IRVariableHeader
{
    int type;
    int offset;
    int count;
    int padding[1];

    char name[IRSDK_MAX_STRING];
    char description[IRSDK_MAX_DESCRIPTION];
    char unit[IRSDK_MAX_STRING];
};

struct IRHeader
{
    int version;
    int status;
    int tick_rate;
    int session_info_update;
    int session_info_length;
    int session_info_offset;
    int variable_count;
    int variable_offset;
    int buffer_count;
    int buffer_length;
    int padding[2];
    IRVariableBuffer buffer[IRSDK_MAX_BUFS];
};

class DataReader
{
public:
    DataReader() : m_file_handle(NULL), m_file_buffer(nullptr)
    {
        Initialize();
    }
    virtual ~DataReader()
    {
        Cleanup();
    }
    bool IsInitialized() const;
    void Initialize();
    std::string Get(const std::string&, size_t = 0) const;
    std::string Get(size_t, size_t = 0) const;
    std::string GetName(size_t) const;
    int GetType(size_t) const;
    int GetCount(size_t) const;
    IRHeader GetHeader() const;
    IRVariableHeader GetVariableHeader(size_t) const;
    std::string GetSessionInfo() const;
    const std::vector<IRVariableHeader>& GetVariableMetadata() const;
private:
    DataReader& operator=(const DataReader&);
    DataReader(const DataReader&);
    void Cleanup();
    void FindTelemetryFile();

    std::unordered_map<std::string, std::pair<int, int>> m_name_to_offset_and_type;
    std::vector<IRVariableHeader> m_cached_metadata;
    HANDLE m_file_handle;
    char* m_file_buffer;
    const static std::wstring m_map_file;
};

#endif // IRTS_DATAREADER_HPP
