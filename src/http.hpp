#ifndef IRTS_HTTP_HPP
#define IRTS_HTTP_HPP

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#undef WIN32_LEAN_AND_MEAN
#include <http.h>

#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <exception>

namespace irts {

class HttpException : public std::exception
{
public:
    HttpException(const char* message, ULONG status) : m_message(message), m_status(status)
    {
        std::ostringstream oss;
        oss.fill('0');
        oss << m_message << ": 0x" << std::hex << std::setw(8) << m_status;
        m_output = oss.str();
    }
    virtual ~HttpException() { }
    HttpException& operator=(const HttpException& r)
    {
        m_message = r.m_message;
        m_status = r.m_status;
        return *this;
    }
    HttpException(const HttpException& r)
    {
        *this = r;
    }
    virtual const char* what() const
    {
        return m_output.c_str();
    }
private:
    std::string m_message;
    std::string m_output;
    ULONG m_status;
};

const std::string g_cache_control_string = "no-cache";

class HttpResponse
{
public:
    HttpResponse(USHORT status_code, const std::string& reason, const std::string& content_type)
        : m_reason(reason), m_content_type(content_type)
    {
        RtlZeroMemory(&m_response, sizeof(m_response));
        m_response.StatusCode = status_code;
        m_response.pReason = m_reason.c_str();
        m_response.ReasonLength = static_cast<USHORT>(m_reason.size());

        m_response.Headers.KnownHeaders[HttpHeaderContentType].pRawValue = m_content_type.c_str();
        m_response.Headers.KnownHeaders[HttpHeaderContentType].RawValueLength = static_cast<USHORT>(m_content_type.size());

        m_response.Headers.KnownHeaders[HttpHeaderCacheControl].pRawValue = g_cache_control_string.c_str();
        m_response.Headers.KnownHeaders[HttpHeaderCacheControl].RawValueLength = static_cast<USHORT>(g_cache_control_string.size());
    }
    virtual ~HttpResponse()
    {
    }
    void AddData(const std::string& text)
    {
        m_data.push_back(text);
        std::string& added_text = *(m_data.end() - 1);
        HTTP_DATA_CHUNK data;

        data.DataChunkType = HttpDataChunkFromMemory;
        data.FromMemory.pBuffer = &added_text[0];
        data.FromMemory.BufferLength = static_cast<ULONG>(added_text.size());

        m_chunks.push_back(data);

        m_response.EntityChunkCount = static_cast<USHORT>(m_chunks.size());
        m_response.pEntityChunks = &m_chunks[0];
    }
    HTTP_RESPONSE* GetPtr()
    {
        return &m_response;
    }
private:
    HttpResponse& operator=(const HttpResponse&);
    HttpResponse(const HttpResponse&);

    std::string m_reason;
    std::string m_content_type;
    std::vector<std::string> m_data;
    std::vector<HTTP_DATA_CHUNK> m_chunks;
    HTTP_RESPONSE m_response;
};


class HttpRequestHandler
{
public:
    virtual void SetRequestQueue(HANDLE req_queue)
    {
        m_req_queue = req_queue;
    }
    virtual DWORD ReceiveRequest()
    {
        // allocates a 2KB + sizeof(HTTP_REQUEST) buffer
        m_buffer.resize(sizeof(HTTP_REQUEST) + 2048);

        m_request = reinterpret_cast<HTTP_REQUEST*>(&m_buffer[0]);

        HTTP_REQUEST_ID request_id;
        HTTP_SET_NULL_ID(&request_id);

        for (;;) {
            RtlZeroMemory(m_request, m_buffer.size());

            DWORD bytes_received;
            ULONG status = HttpReceiveHttpRequest(
                                m_req_queue,
                                request_id,
                                0,
                                m_request,
                                static_cast<ULONG>(m_buffer.size()),
                                &bytes_received,
                                nullptr
                                );

            if (status != NO_ERROR) {
                throw HttpException("HttpReceiveHttpRequest failed", status);
            }

//            cout << "Received " << bytes_received << " bytes" << endl;
//            cout << "Request type: " << m_request->Verb << endl;

            ProcessRequest(m_request);

        }

        return 0;
    }
    virtual DWORD SendXmlData(const std::string& xml) const
    {
        HttpResponse response(200, "OK", "text/xml");
        response.AddData(xml);

        return SendResponse(response);
    }
    virtual DWORD SendJsonData(const std::string& json) const
    {
        HttpResponse response(200, "OK", "application/json");
        response.AddData(json);

        return SendResponse(response);
    }
    virtual DWORD SendHtmlData(const std::string& html) const
    {
        HttpResponse response(200, "OK", "text/html");
        response.AddData(html);

        return SendResponse(response);
    }
    virtual DWORD ProcessRequest(const HTTP_REQUEST*) = 0;
private:
    DWORD SendResponse(HttpResponse& r) const
    {
        return HttpSendHttpResponse(
                    m_req_queue,
                    m_request->RequestId,
                    0,
                    r.GetPtr(),
                    nullptr,
                    nullptr,
                    nullptr,
                    0,
                    nullptr,
                    nullptr
                    );
    }

    HTTP_REQUEST* m_request;
    std::vector<char> m_buffer;
    HANDLE m_req_queue;
};

template <class Handler>
class HttpListener
{
public:
    HttpListener(const std::wstring& url, bool print_variables)
        : m_req_queue(NULL), m_url(url), m_handler(print_variables)
    {
        Initialize();
        Listen();
    }
    virtual ~HttpListener()
    {
        Cleanup();
    }
private:
    HttpListener& operator=(const HttpListener&);
    HttpListener(const HttpListener&);
    void Initialize()
    {
        HTTPAPI_VERSION http_version = HTTPAPI_VERSION_2;

        ULONG status = HttpInitialize(
                            http_version,
                            HTTP_INITIALIZE_SERVER,
                            nullptr
                            );
        if (status != NO_ERROR) {
            throw HttpException("HttpInitialize failed", status);
        }

        status = HttpCreateHttpHandle(
                    &m_req_queue,
                    0
                    );

        if (status != NO_ERROR) {
            throw HttpException("HttpCreateHttpHandle failed", status);
        }

        status = HttpAddUrl(
                    m_req_queue,
                    m_url.c_str(),
                    NULL
                    );

        if (status != NO_ERROR) {
            throw HttpException("HttpAddUrl failed", status);
        }
    }

    void Listen()
    {
        m_handler.SetRequestQueue(m_req_queue);
        m_handler.ReceiveRequest();
    }

    void Cleanup()
    {
        HttpRemoveUrl(
            &m_req_queue,
            m_url.c_str()
            );

        if (m_req_queue) {
            CloseHandle(m_req_queue);
        }

        HttpTerminate(HTTP_INITIALIZE_SERVER, nullptr);
    }

    Handler m_handler;
    HANDLE m_req_queue;
    std::wstring m_url;
};

} // namespace irts

#endif // IRTS_HTTP_HPP
