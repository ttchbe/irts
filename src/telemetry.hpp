#ifndef IRTS_TELEMETRY_HPP
#define IRTS_TELEMETRY_HPP

#include "http.hpp"

#include <string>
#include <sstream>

extern std::string GetXmlForSession(const std::string&);
extern std::string GetJsonForSession(const std::string&);

namespace irts {

class TelemetryRequestHandler : public HttpRequestHandler
{
public:
    TelemetryRequestHandler(bool print_variables)
        : m_print_variables(print_variables)
    {
    }
    virtual ~TelemetryRequestHandler()
    {
    }
    virtual DWORD ProcessRequest(const HTTP_REQUEST* request)
    {
        if (!m_dr.IsInitialized()) {
            m_dr.Initialize();
        }
        bool do_json = true;
        if (request->CookedUrl.QueryStringLength) {
            do_json = lstrcmpiW(request->CookedUrl.pQueryString, L"?xml") != 0;
        }
        if (do_json) {
            DoJsonResponse();
        } else {
            DoXmlResponse();
        }

        return 0;
    }
private:
    void DoXmlResponse() const
    {
        std::ostringstream xml;
        xml << "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
        xml << "\n<telemetry>\n";
        for (size_t i = 0; i != m_dr.GetVariableMetadata().size(); ++i) {
            if (m_dr.GetCount(i) == 1) {
                xml << "\t<" << m_dr.GetName(i) << " value=\"" << m_dr.Get(i) << "\"/>\n";
            } else {
                xml << "\t<" << m_dr.GetName(i) << ">\n";
                for (size_t j = 0; j != static_cast<size_t>(m_dr.GetCount(i)); ++j) {
                    xml << "\t\t<Entry index=\"" << j << "\""
                        << " value=\"" << m_dr.Get(i, j) << "\""
                        << "/>\n";
                }
                xml << "\t</" << m_dr.GetName(i) << ">\n";
            }

        }
        xml << "<rawsession><![CDATA[\n";
        xml << m_dr.GetSessionInfo() << "\n";
        xml << "]]></rawsession>\n";

        xml << GetXmlForSession(m_dr.GetSessionInfo());

        if (m_print_variables) {
            xml << "\n\t<Variables>\n";
            for (const auto& variable : m_dr.GetVariableMetadata()) {
                xml << "\t\t<Variable"
                    << " name=\"" << std::string(variable.name) << "\""
                    << " description=\"" << std::string(variable.description) << "\""
                    << " units=\"" << std::string(variable.unit) << "\""
                    << " type=\"" << variable.type << "\""
                    << " count=\"" << variable.count << "\""
                    << "/>\n";
            }
            xml << "\t</Variables>\n";
        }
        xml << "</telemetry>\n";
        ULONG status = SendXmlData(xml.str());
        if (status != NO_ERROR) {
            throw HttpException("HttpSendHttpResponse failed", status);
        }

    }

    void PutValueForJson(std::ostringstream& json, size_t index, size_t sub_index = 0) const
    {
        if (m_dr.GetType(index) == IRBitField) {
            json << "\"";
        }
        json << m_dr.Get(index, sub_index);
        if (m_dr.GetType(index) == IRBitField) {
            json << "\"";
        }
    }

    void DoJsonResponse() const
    {
        std::ostringstream json;
        json << "{\n";
        for (size_t i = 0; i != m_dr.GetVariableMetadata().size(); ++i) {
            json << "\t\"" << m_dr.GetName(i) << "\":";
            if (m_dr.GetCount(i) == 1) {
                PutValueForJson(json, i);
            } else {
                json << "[";
                for (size_t j = 0; j != static_cast<size_t>(m_dr.GetCount(i)); ++j) {
                    PutValueForJson(json, i, j);
                    if (j != static_cast<size_t>(m_dr.GetCount(i)) - 1) {
                        json << ",";
                    }
                }
                json << "]";
            }

            json << ",\n";
        }

        json << GetJsonForSession(m_dr.GetSessionInfo());

        if (m_print_variables) {
            json << ", \"variables\" : [\n";
            for (size_t i = 0; i != m_dr.GetVariableMetadata().size(); ++i) {
                auto variable = m_dr.GetVariableHeader(i);
                json << "\t{\"" << std::string(variable.name) << "\":\n"
                    << "\t\t{\"description\" : " << "\"" << std::string(variable.description) << "\",\n"
                    << "\t\t\"units\" : " << "\"" << std::string(variable.unit) << "\",\n"
                    << "\t\t\"type\" : " << variable.type << ",\n"
                    << "\t\t\"count\" : " << variable.count << "}\n\t}";
                if (i != m_dr.GetVariableMetadata().size() - 1) {
                    json << ",";
                }
                json << "\n";
            }
            json << "]\n";
        }
        json << "}\n";

        ULONG status = SendJsonData(json.str());
        if (status != NO_ERROR) {
            throw HttpException("HttpSendHttpResponse (SendJsonData) failed", status);
        }
    }

    bool m_print_variables;
    DataReader m_dr;
};

} // namespace irts

#endif // IRTS_TELEMETRY_HPP
