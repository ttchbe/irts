#include <windows.h>
#include <stdio.h>

VOID SvcInstall()
{
    SC_HANDLE sc_manager;
    SC_HANDLE sc_service;

    wchar_t path[MAX_PATH];

    if (!GetModuleFileNameW(nullptr, path, MAX_PATH)) {
        printf("Service installation failed (%d)\n", GetLastError());
        return;
    }

    sc_manager = OpenSCManager(
        nullptr,
        nullptr,
        SC_MANAGER_ALL_ACCESS
        );

    if (NULL == sc_manager) {
        printf("OpenSCManager failed (%d)\n", GetLastError());
        return;
    }

    sc_service = CreateServiceW(
        sc_manager,
        L"irts",
        L"Telemetry Server for iRacing",
        SERVICE_ALL_ACCESS,
        SERVICE_WIN32_OWN_PROCESS,
        SERVICE_AUTO_START,
        SERVICE_ERROR_NORMAL,
        path,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr
        );

    if (NULL == sc_service) {
        printf("CreateService failed (%d)\n", GetLastError());
    } else {
        printf("Service installed successfully\n");
    }

    CloseServiceHandle(sc_service);
    CloseServiceHandle(sc_manager);
}

VOID __stdcall DoDeleteSvc()
{
    SC_HANDLE sc_manager;
    SC_HANDLE sc_service;

    sc_manager = OpenSCManager(
        nullptr,
        nullptr,
        SC_MANAGER_ALL_ACCESS
        );

    if (NULL == sc_manager) {
        printf("OpenSCManager failed (%d)\n", GetLastError());
        return;
    }

    sc_service = OpenServiceW(
        sc_manager,
        L"irts",
        DELETE
        );

    if (!DeleteService(sc_service)) {
        printf("DeleteService failed (%d)\n", GetLastError());
    } else {
        printf("Service deleted successfully\n");
    }

    CloseServiceHandle(sc_service);
    CloseServiceHandle(sc_manager);
}
