CPPFLAGS=/nologo /EHsc /W4 /WX /Zi /GL /MT /O2
INCLUDES=$(INCLUDES) /I..\..\yaml-cpp\include /I..\..\boost_1_52_0
LINKFLAGS=/Debug /Release /LTCG
LINKLIBS=httpapi.lib secur32.lib advapi32.lib libyaml-cppmt.lib
LIBPATH_X64=/libpath:..\..\yaml-cpp\x64\Release
LIBPATH_X86=/libpath:..\..\yaml-cpp\x86\Release
CANDLE="C:\Program Files (x86)\WiX Toolset v3.7\bin\candle.exe"
LIGHT="C:\Program Files (x86)\WiX Toolset v3.7\bin\light.exe"

CPPFILES=\
	src\main.cpp \
	src\datareader.cpp \
	src\install.cpp \
	src\session.cpp

bin\x64\irts.exe: $(CPPFILES)
	-@ if NOT EXIST bin\x64 md bin\x64
	-@ if NOT EXIST obj\x64 md obj\x64
	cmd /c "$(VCINSTALLDIR)\vcvarsall x86_amd64 && $(CPP) $** $(CPPFLAGS) $(INCLUDES) /Foobj\x64\ /Fdbin\x64\ /Fe$@ /link $(LINKFLAGS) $(LIBPATH_X64) $(LINKLIBS)"

bin\x86\irts.exe: $(CPPFILES)
	-@ if NOT EXIST bin\x86 md bin\x86
	-@ if NOT EXIST obj\x86 md obj\x86
	cmd /c "$(VCINSTALLDIR)\vcvarsall x86 && $(CPP) $** $(CPPFLAGS) $(INCLUDES) /Foobj\x86\ /Fdbin\x86\ /Fe$@ /link $(LINKFLAGS) $(LIBPATH_X86) $(LINKLIBS)"

setup\x64\irts64.msi:
	$(CANDLE) -nologo -arch x64 -darch=x64 -o setup\x64\irts64.wixobj setup\irts.wxs
	$(LIGHT) -nologo -ext WixUIExtension -cultures:en-us -o setup\x64\irts64.msi setup\x64\irts64.wixobj

setup\x86\irts32.msi:
	$(CANDLE) -nologo -arch x86 -darch=x86 -o setup\x86\irts32.wixobj setup\irts.wxs
	$(LIGHT) -nologo -ext WixUIExtension -cultures:en-us -o setup\x86\irts32.msi setup\x86\irts32.wixobj

irts64: bin\x64\irts.exe

irts32: bin\x86\irts.exe

setup64: setup\x64\irts64.msi

setup32: setup\x86\irts32.msi

setup: setup32 setup64

all: irts32 irts64 setup32 setup64
